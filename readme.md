**Maze Haze**
===================

**Theme & Backstory** 
  -----------------

The player will start at the begging of a maze house that they need to work their way through to escape. The player will wake up in this house and the house itself will have a spooky other worldy type feel to it. The enemies that the player comes across in the house will be spirits that are trapped in the house. 

**World & Location**
  ----------------

The setting will be a house with a maze like structure with a spooky feel to it. There will also be "boss" rooms that have riddles or puzzles the player must solve. The bosses will be evil spirits and ghosts that haunt the house.

**Items & Objects**
  ---------------
Possible items can be clues that can help them solve an up comming riddle or a map so they can keep track of where they've been.

**Goals & Obstacles**
  -----------------

The ultimate goal is to escape the house. Major steps are navigating the maze to reach boss rooms where they have to solve a riddle or question in a certain amount of trys or be sent back to the beginning of the game. 

**Essential Types & Classes**
  -------------------------
Types I will make:

1. Action
2. Item
3. Player

The player will have to solve problems which will require booleans. Need to keep track of how many wrong answers. Have to keep track of a map with lists.  

**Function Design**
  ---------------

getAction :: String -> Action

pickup :: Item -> [a] -> [a]

removeFromRoom :: Item -> [a] -> [a]

dropOnUse :: Item -> [a] -> [a]

useClueItem :: Item -> String

displayMap :: [[a]] -> String
	
*	String will tell the player what door they came from and the options that they have already been to. 

examine :: [a] -> String

doMove :: [[a]] -> [[a]]


